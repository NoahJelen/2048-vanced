#!/bin/sh
bash build.sh
echo -e '#!/bin/sh\n/opt/v2048/v2048'>v2048.sh
sudo cp -r v2048/ /opt/v2048
sudo mkdir -p /opt/v2048/data
sudo chmod -R 777 /opt/v2048/data
sudo cp v2048.desktop /usr/share/applications/v2048.desktop
sudo cp v2048.sh /usr/bin/v2048
sudo chmod 755 /usr/bin/v2048
sudo chmod 755 /opt/v2048/v2048