[![AUR version](https://img.shields.io/aur/version/2048-vanced)](https://aur.archlinux.org/packages/2048-vanced/)
# 2048 Vanced
![](icon.png)

2048 Vanced is yet another implementation of the game [2048](https://github.com/gabrielecirulli/2048)

![](screenshots/game.png)

Note: This game is still in its early stages. Things can and will break until it reaches 1.0. You can report bugs by opening an issue on this repo or by emailing [incoming+noahjelen-2048-vanced-25487050-issue-@incoming.gitlab.com](mailto:incoming+noahjelen-2048-vanced-25487050-issue-@incoming.gitlab.com). When reporting an issue, run `v2048 >log.txt 2>&1` then try to reproduce. When reporting your issue, attach log.txt to it.

# Screenshots

## Loading Screen
![](screenshots/loading_screen.png)

## Main Menu
![](screenshots/menu.png)

## In game
![](screenshots/game.png)

# How to build (and install) 2048 Vanced

## Arch Linux based Linux systems
2048 Vanced is available in the AUR for Arch Linux and any system based on it (like Manjaro Linux, EndeavourOS, and Artix Linux)

Installation example using `yay`: `yay -S 2048-vanced`

## Other Linux based systems

Make sure you have the latest version of Rust installed

Instructions on how to install it are [here](https://rustup.rs/)

After installing Rust run the following commands:

`git clone https://gitlab.com/NoahJelen/2048-vanced`

`cd 2048-vanced`

`./install-linux.sh`  <-- This will request root access in order to install the program

## Microsoft Windows

Build Instructions for Windows is coming soon. I plan to have 2048 Vanced support Windows 7 (I really don't care that Microsoft declared EOL on it) and later.

## macOS X

If you're trying to build on macOS, you're on your own since I do not support Apple and their products.


After the build is complete, you will get a directory called "v2048", move/copy this directory to wherever you want it go. The game, its saves, and its support files are all in this folder.

To do:
- [ ] Support for various resolutions (not just 720p)
- [ ] Mouse support
- [ ] Support for Windows 7 and later
- [ ] Logging system to users report bugs
- [ ] Animations
- [ ] Scoreboard for all users
- [ ] Android 8+ support
- [ ] Release game on itch.io
- [ ] Boards with Hexagonal and Triangular tiles
- [ ] 3D 2048