use std::{env, fs};

use rust_utils::linux::{
    DesktopEntryFileBuilder,
    AurPkgbuildBuilder
};

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let profile = env::var("PROFILE").unwrap();
    if profile == "release" {
        fs::write(
            "v2048.sh",
            "#!/bin/sh\n\
            /opt/v2048/v2048"
        ).unwrap();
    }

    DesktopEntryFileBuilder::new(
        "2048 Vanced",
        "/opt/v2048/v2048",
        false,
        VERSION
    )
        .comment("An advanced implementation of 2048")
        .icon("/opt/v2048/assets/icon.png")
        .category("Game")
        .build("v2048.desktop");

    AurPkgbuildBuilder::new(
        "2048-vanced",
        VERSION,
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "An advanced implementation of 2048",
        "https://gitlab.com/NoahJelen/2048-vanced/-/archive/$pkgver/2048-vanced-$pkgver.zip",
        "https://gitlab.com/NoahJelen/2048-vanced",
        "GPL3",
        "    cd \"2048-vanced-$pkgver\"\n    \
        bash build.sh",
        "    cd \"2048-vanced-$pkgver\"\n    \
        mkdir -p \"$pkgdir/usr/share/applications/\"\n    \
        mkdir -p \"$pkgdir/usr/bin/\"\n    \
        mkdir -p \"$pkgdir/opt/\"\n    \
        cp -r v2048/ \"$pkgdir/opt/v2048\"\n    \
        cp v2048.desktop \"$pkgdir/usr/share/applications/v2048.desktop\"\n    \
        cp v2048.sh \"$pkgdir/usr/bin/v2048\"\n    \
        chmod 755 \"$pkgdir/usr/bin/v2048\"\n    \
        chmod 755 \"$pkgdir/opt/v2048/v2048\""
    )
        .dependency("gcc-libs")
        .dependency("glibc")
        .make_dependency("cargo")
        .make_dependency("gzip")
        .build("./aur");
}