#!/bin/sh
# Build script for 2048 Vanced
mkdir -p .git/refs/heads
echo "Release">.git/refs/heads/master
cargo build --release
mkdir v2048
cp target/release/v2048 v2048/v2048
cp -r assets v2048/assets