use std::process;
use glutin_window::{GlutinWindow, UserEvent};
use opengl_graphics::OpenGL;
use piston_window::{
    clear,
    rectangle,
    types::Color,
    Button, EventLoop,
    Key, PistonWindow,
    PressEvent, ResizeEvent,
    Window, WindowSettings,
    color::{
        self,
        BLACK, GREEN,
        RED, WHITE
    },
};
use rand::Rng;
use rust_utils::config::Config;
use winit::{
    dpi::LogicalSize,
    event_loop::EventLoopBuilder,
    window::{Icon, WindowBuilder}
};
use crate::{
    game::{BoardSelector, BoardWidget},
    options::Options,
    ui::{
        Backdrop, HAlignment, ImageWidget, SelectWidget, TextWidget, VAlignment, Widget
    }
};
use image::io::Reader as ImageReader;

mod ui;
mod tiles;
mod game;
mod options;

const VERSION: &str = env!("CARGO_PKG_VERSION");

// custom colors
const SILVER: Color = [0.5019608, 0.5019608, 0.5019608, 0.25];
const AQUA: Color = [0.262745098, 0.6549019608, 0.6588235294, 1.];

static BACKDROPS: [&str; 11] = [
    "andromeda.png",
    "earth.png",
    "galaxies1.png",
    "galaxies2.png",
    "galaxy_and_star.png",
    "magellanic_cloud.png",
    "nebula1.png",
    "nebula2.png",
    "saturn.png",
    "star1.png",
    "star2.png"
];

// for dev builds, show the hash of the last commit
#[cfg(debug_assertions)]
const LATEST_COMMIT: &str = include_str!("../.git/refs/heads/master");

// possible screens for the game
#[derive(Debug, Copy, Clone, PartialEq)]
enum Screen {
    Game,
    Options,
    MainMenu
}

fn main() {
    // the assets directory
    let assets = game::get_game_dir().join("assets");

    // generate the initial window
    let mut window = genwin();
    
    // create the texture context for the game (used for importing images)
    let mut tc = window.create_texture_context();

    // the font used in the game
    let font_path = assets.join("fonts/OCRA.ttf");
    let mut font = window.load_font(&font_path).unwrap();

    // title screen
    let logo = ImageWidget::new(assets.join("aercloud_systems.png"), &mut tc);
    let banner = ImageWidget::new(assets.join("banner.png"), &mut tc);

    while let Some(event) = window.next() {
        if let Some(Button::Keyboard(_)) = event.press_args() { break; }
        let win_size: [f64; 2] = window.size().into();
        window.draw_2d(&event, |c, g, d| {
            clear(BLACK, g);
            banner.draw_aligned(HAlignment::Center, VAlignment::Absolute(104.), win_size, 0., 0., &mut font, c, g, d);
            logo.draw_aligned(HAlignment::Right, VAlignment::Bottom, win_size, 20., 20., &mut font, c, g, d);
            TextWidget::new("Press any key", color::hex("009e92"), 55, None)
                .draw_aligned(HAlignment::Center, VAlignment::Bottom, win_size, 0., 30., &mut font, c, g, d);
        });
    };
    
    // get the backdrop image
    let mut random = rand::thread_rng();
    let backdrop_dir = assets.join("backdrops/");
    let backdrop_path = backdrop_dir.join(BACKDROPS[random.gen_range(0..11)]);
    let mut backdrop = Backdrop::new(&backdrop_path, window.size().into(), &mut tc);
    let mut options = Options::load();
    let mut game_board = BoardWidget::new(options.board_size);
    game_board.resize(window.size().into(), &mut tc, &mut font);

    let mut screen = Screen::MainMenu;
    let mut game_menu = SelectWidget::new(50, WHITE, SILVER, AQUA)
        .item("Play")
        .item("Quit");

    let mut board_select = BoardSelector::new(WHITE, SILVER, AQUA);

    // Options menu:
    // 0: Colorblind mode toggle
    // 1: Main Menu
    // 2: Reset game
    // 3: Quit

    // Main menu:
    // 0: Play game with selected board size
    // 1: Exit
    let mut selected_option = 0;

    while let Some(event) = window.next() {
        let win_size: [f64; 2] = window.size().into();
        backdrop.resize(win_size, &mut tc);
        game_menu.on_event(&event, &mut tc, &mut font);
        board_select.on_event(&event, &mut tc, &mut font);
        board_select.refresh_image(&mut tc);

        if let Some(Button::Keyboard(key)) = event.press_args() {
            let selected_board_option = game_menu.selected();
            let board_size = board_select.size();

            // event loop for the main menu
            if screen == Screen::MainMenu {
                if board_select.refresh_image(&mut tc) {
                    game_board.reload(board_size);
                    options.board_size = board_size;
                    options.save().unwrap();
                }

                if key == Key::Return {
                    game_board.reload(board_size);
                    game_board.resize(window.size().into(), &mut tc, &mut font);
                    screen = Screen::Game
                }

                // exit the game
                if selected_board_option == 1 && key == Key::Return {
                    process::exit(0);
                }
            }

            // event loop for the game
            if screen == Screen::Game {
                game_board.on_event(&event, &mut tc, &mut font);

                if key == Key::O {
                    screen = Screen::Options
                }
            }

            // event loop for the options menu
            if screen == Screen::Options {
                // go back to the game
                if key == Key::Escape {
                    screen = Screen::Game;
                }

                // move down
                if key == Key::Down {
                    selected_option += 1;
                    if selected_option >= 4 {
                        selected_option = 0;
                    }
                }
                
                // move up
                if key == Key::Up {
                    selected_option -= 1;
                    if selected_option < 0 {
                        selected_option = 3;
                    }
                }

                // turn color blind mode on or off
                if selected_option == 0 {
                    if key == Key::Left {
                        options.color_blind_mode = options.color_blind_mode.prev();
                    }
                    if key == Key::Right || key == Key::Return {
                        options.color_blind_mode = options.color_blind_mode.next();
                    }
                }

                // go to the main menu
                if selected_option == 1 {
                    if key == Key::Return {
                        screen = Screen::MainMenu;
                    }
                }

                // reset game
                if selected_option == 2 && key == Key::Return {
                    let data = game_board.data_mut();
                    data.reset();
                    selected_option = 0;
                    screen = Screen::Game;
                }

                // quit and save the game
                if selected_option == 3 && key == Key::Return {
                    let data = game_board.data();
                    data.save();
                    options.save().unwrap();
                    process::exit(0);
                }
            }

            options.save().unwrap();
        }

        if let Some(resize_event) = event.resize_args() {
            let new_size = resize_event.window_size;
            game_board.resize(new_size, &mut tc, &mut font)
        }

        window.draw_2d(&event, |c, g, d| {
            backdrop.draw(c, g);

            // render the main menu
            if screen == Screen::MainMenu {
                // select board title
                TextWidget::new("Select Board", WHITE, 50, Some(SILVER))
                    .draw_aligned(HAlignment::Center, VAlignment::Top, win_size, 0., 28., &mut font, c, g, d);

                board_select.draw_aligned(HAlignment::Center, VAlignment::Absolute(145.), win_size, 0., 0., &mut font, c, g, d);

                // best score of selected size
                TextWidget::new(board_select.get_best_score(), WHITE, 40, Some(SILVER))
                    .draw_aligned(HAlignment::Center, VAlignment::Bottom, win_size, 0., 10., &mut font, c, g, d);

                // play game or quit?
                game_menu.draw_aligned(HAlignment::Center, VAlignment::Absolute(540.), win_size, 0., 0., &mut font, c, g, d);
            }

            // render the game
            if screen == Screen::Game {
                game_board.draw_aligned(HAlignment::Right, VAlignment::Bottom, win_size, 5., 5., &mut font, c, g, d);
                let data = game_board.data();

                // is the game over?
                if data.is_over() {
                    rectangle(SILVER, [640., 313., 370., 77.], c.transform, g);
                    game::show_text(640., 360., "Game over!", RED, 50, &mut font, c, g, d);
                    game::show_text(650., 382., "Press R for a new game", WHITE, 22, &mut font, c, g, d);
                }

                // did the user win (has a 2048 tile)?
                if data.won() && !data.saw_won_scr() {
                    rectangle(SILVER, [590., 313., 620., 77.], c.transform, g);
                    game::show_text(590., 360., "You are a winner!", GREEN, 50, &mut font, c, g, d);
                    game::show_text(750., 382., "Press Esc to continue", WHITE, 22, &mut font, c, g, d);
                }

                // the score and best score
                TextWidget::new(format!("Score: {}", data.score()), WHITE, 50, Some(SILVER))
                    .draw_aligned(HAlignment::Left, VAlignment::Absolute(25.), win_size, 25., 0., &mut font, c, g, d);

                TextWidget::new(format!("Best: {}", data.best()), WHITE, 50, Some(SILVER))
                    .draw_aligned(HAlignment::Left, VAlignment::Absolute(105.), win_size, 25., 0., &mut font, c, g, d);

                // how to play text
                TextWidget::new(
                    "U: Undo move\n\
                    R: New Game\n\
                    W, A, S, D,\n\
                    and Keyboard Arrows: Move tiles\n\
                    O: Options Menu",
                    WHITE, 22, None
                )
                    .draw_aligned(HAlignment::Left, VAlignment::Absolute(200.), win_size, 25., 0., &mut font, c, g, d);

                // version info
                TextWidget::new(format!("Version {VERSION}"), WHITE, 22, None)
                    .draw_aligned(HAlignment::Left, VAlignment::Bottom, win_size, 25., 50., &mut font, c, g, d);

                // debug info
                #[cfg(debug_assertions)]
                TextWidget::new(LATEST_COMMIT, WHITE, 10, None)
                    .draw_aligned(HAlignment::Left, VAlignment::Bottom, win_size, 25., 20., &mut font, c, g, d);
            }

            // render the options menu
            if screen == Screen::Options {
                rectangle(SILVER, [10., 10., 1260., 700.], c.transform, g);
                game::show_text(280., 90., "Options Menu", WHITE, 75, &mut font, c, g, d);

                if selected_option == 0 {
                    game::show_text(10., 150., format!("> Colorblind Mode: < {} >",options.color_blind_mode.desc()), WHITE, 50, &mut font, c, g, d);
                }
                else {
                    game::show_text(10., 150., format!("  Colorblind Mode: {}",options.color_blind_mode.desc()), WHITE, 50, &mut font, c, g, d);
                }

                if selected_option == 1 {
                    game::show_text(10., 200., "> Main Menu", WHITE, 50, &mut font, c, g, d);
                }
                else {
                    game::show_text(10., 200., "  Main Menu", WHITE, 50, &mut font, c, g, d);
                }

                if selected_option == 2 {
                    game::show_text(10., 250., "> Reset Game", WHITE, 50, &mut font, c, g, d);
                }
                else {
                    game::show_text(10., 250., "  Reset Game", WHITE, 50, &mut font, c, g, d);
                }

                if selected_option == 3 {
                    game::show_text(10., 300., "> Quit Game", WHITE, 50, &mut font, c, g, d);
                }
                else {
                    game::show_text(10., 300., "  Quit Game", WHITE, 50, &mut font, c, g, d);
                }
            }
        });
    }
}

// generates the window with its icon
fn genwin() -> PistonWindow {
    let game_dir = game::get_game_dir();

    // the icon image
    let icon_image = ImageReader::open(game_dir.join("assets/icon.png")).unwrap().with_guessed_format().unwrap().decode().unwrap();
    let image_bytes = icon_image.as_bytes();
    let icon = Icon::from_rgba(image_bytes.to_vec(), 256, 256).unwrap();

    // event loop
    let eloop = EventLoopBuilder::<UserEvent>::with_user_event().build();

    // the raw winit window
    let win_raw = WindowBuilder::new().with_window_icon(Some(icon)).with_resizable(true).with_inner_size(LogicalSize::new(1280,720));

    // the window's settings
    let win_cfg = WindowSettings::new(format!("2048 Vanced {VERSION}"), [1280, 720]);

    // create the window
    let gl_win = GlutinWindow::from_raw(&win_cfg, eloop, win_raw.with_title(format!("2048 Vanced {VERSION}"))).unwrap();

    PistonWindow::new(OpenGL::V4_5, 0, gl_win).lazy(false)
}