use super::Widget;
use std::path::PathBuf;
use gfx_device_gl::Device;
use image::{
    imageops::FilterType,
    io::Reader as ImageReader
};
use opengl_graphics::TextureSettings;
use piston_window::{
    image as draw_image,
    Context,
    G2d, G2dTexture,
    G2dTextureContext,
    Glyphs, Texture,
    Transformed
};

pub struct ImageWidget {
    size: [f64; 2],
    img_buf: G2dTexture
}

impl ImageWidget {
    pub fn new_sized<P: Into<PathBuf>>(path: P, size: [f64; 2], tc: &mut G2dTextureContext) -> Self {
        let path = path.into();
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(size[0] as u32, size[1] as u32, FilterType::Lanczos3);

        ImageWidget {
            size,
            img_buf: Texture::from_image(
                tc,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap()
        }
    }

    pub fn new<P: Into<PathBuf>>(path: P, tc: &mut G2dTextureContext) -> Self {
        let path = path.into();
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap();

        let size = [img.width() as f64, img.height() as f64];

        ImageWidget {
            size,
            img_buf: Texture::from_image(
                tc,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap()
        }
    }
}

impl Widget for ImageWidget {
    fn draw(&self, x: f64, y: f64, _font: &mut Glyphs, ctx: Context, gfx: &mut G2d, _dev: &mut Device) {
        draw_image(&self.img_buf, ctx.transform.trans(x, y), gfx);
    }

    fn size(&self, _max: [f64; 2], _font: &mut Glyphs) -> [f64; 2] { self.size }
}