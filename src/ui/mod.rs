//TODO: once complete, this module will
// be broken off into a new crate called aerui (Aercloud UI)
// and will be its own GUI library that will have a syntax similar to cursive
use std::path::PathBuf;
use image::{imageops::FilterType, io::Reader as ImageReader};
use gfx_device_gl::Device;
use opengl_graphics::TextureSettings;
use piston_window::{
    clear, image as draw_image, Context, Event, G2d, G2dTexture, G2dTextureContext, Glyphs, ResizeEvent, Texture
};

mod text_widget;
mod select_widget;
mod image_widget;

pub use text_widget::TextWidget;
pub use select_widget::SelectWidget;
pub use image_widget::ImageWidget;

pub trait Widget: 'static {
    fn draw(&self, x: f64, y: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device);
    fn on_event(&mut self, _event: &Event, _tc: &mut G2dTextureContext, _font: &mut Glyphs) { }
    fn resize(&mut self, _new_max: [f64; 2], _tc: &mut G2dTextureContext, _font: &mut Glyphs) { }
    fn size(&self, _max: [f64; 2], _font: &mut Glyphs) -> [f64; 2] { [1., 1.] }

    fn draw_aligned(&self, h_align: HAlignment, v_align: VAlignment, win_size: [f64; 2], h_margin: f64, v_margin: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        let elem_size = self.size(win_size, font);
        
        self.draw(
            h_align.calc(win_size[0], elem_size[0], h_margin),
            v_align.calc(win_size[1], elem_size[1], v_margin),
            font, ctx, gfx, dev
        )
    }
}

struct PositionedWidget {
    h_align: HAlignment,
    h_margin: f64,
    v_align: VAlignment,
    v_margin: f64,
    elem: Box<dyn Widget>
}

pub struct Backdrop {
    path: PathBuf,
    img_buf: G2dTexture,
    win_size: [f64; 2]
}

impl Backdrop {
    pub fn new<P: Into<PathBuf>>(path: P, win_size: [f64; 2], tc: &mut G2dTextureContext) -> Self {
        let path = path.into();
        let img = ImageReader::open(&path).unwrap()
            .with_guessed_format().unwrap()
            .decode().unwrap()
            .resize_exact(win_size[0] as u32, win_size[1] as u32, FilterType::Lanczos3);

        Backdrop {
            path,
            img_buf: Texture::from_image(
                tc,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap(),
            win_size
        }
    }

    pub fn resize(&mut self, new_size: [f64; 2], tc: &mut G2dTextureContext) {
        if new_size != self.win_size {
            let img = ImageReader::open(&self.path).unwrap()
                .with_guessed_format().unwrap()
                .decode().unwrap()
                .resize_exact(new_size[0] as u32, new_size[1] as u32, FilterType::Lanczos3);

            self.img_buf = Texture::from_image(
                tc,
                &img.to_rgba8(),
                &TextureSettings::new()
            ).unwrap();

            self.win_size = new_size;
        }
    }

    pub fn draw(&self, ctx: Context, gfx: &mut G2d) {
        clear([0.; 4], gfx);
        draw_image(&self.img_buf, ctx.transform, gfx);
    }
}

pub struct GameUI {
    screen: Screen,
    elements: Vec<PositionedWidget>
}

impl GameUI {
    pub fn new() -> Self {
        GameUI {
            screen: Screen::MainMenu,
            elements: vec![]
        }
    }

    pub fn add_element<E: Widget>(&mut self, element: E, x: f64, y: f64) {
        self.add_aligned_element(element, HAlignment::Absolute(x), 0., VAlignment::Absolute(y), 0.);
    }

    pub fn add_aligned_element<E: Widget>(&mut self, element: E, h_align: HAlignment, h_margin: f64, v_align: VAlignment, v_margin: f64) {
        self.elements.push(
            PositionedWidget {
                h_align,
                h_margin,
                v_align,
                v_margin,
                elem: Box::new(element)
            }
        );
    }

    pub fn draw(&self, win_size: [f64; 2], font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        for PositionedWidget { h_align, h_margin, v_align, v_margin, elem } in &self.elements {
            elem.draw_aligned(*h_align, *v_align, win_size, *h_margin, *v_margin, font, ctx, gfx, dev);
        }
    }

    pub fn on_event(&mut self, event: &Event, tc: &mut G2dTextureContext, font: &mut Glyphs) {
        for PositionedWidget { elem, .. } in  &mut self.elements {
            elem.on_event(event, tc, font);
            if let Some(resize_event) = event.resize_args() {
                elem.resize(resize_event.window_size, tc, font)
            }
        }
    }
}

// possible screens for the game
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Screen {
    Game,
    Options,
    MainMenu
}

#[derive(Copy, Clone)]
pub enum HAlignment {
    Absolute(f64),
    Left,
    Center,
    Right
}

impl HAlignment {
    fn calc(self, width: f64, elem_width: f64, margin: f64) -> f64 {
        match self {
            Self::Absolute(x) => x,
            Self::Left => margin,
            Self::Right => width - elem_width - margin,
            Self::Center => (width / 2.) - (elem_width / 2.)
        }
    }
}

#[derive(Copy, Clone)]
pub enum VAlignment {
    Absolute(f64),
    Top,
    Center,
    Bottom
}

impl VAlignment {
    fn calc(self, height: f64, elem_height: f64, margin: f64) -> f64 {
        match self {
            Self::Absolute(y) => y,
            Self::Top => margin,
            Self::Bottom => height - elem_height - margin,
            Self::Center => (height / 2.) - (elem_height / 2.)
        }
    }
}