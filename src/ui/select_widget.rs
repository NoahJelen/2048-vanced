use std::fmt::Display;

use gfx_device_gl::Device;
use piston_window::{
    polygon, types::Color, Button, CharacterCache, Context, DrawState, Event, G2d, G2dTextureContext, Glyphs, Key, PressEvent, Rectangle, Text
};
use super::Widget;

pub struct SelectWidget {
    selected_idx: usize,
    elements: Vec<String>,
    font_size: u32,
    text_color: Color,
    box_color: Color,
    arrow_color: Color
}

impl SelectWidget {
    pub fn new(font_size: u32, text_color: Color, box_color: Color, arrow_color: Color) -> Self {
        SelectWidget {
            selected_idx: 0,
            elements: vec![],
            font_size,
            text_color,
            box_color,
            arrow_color
        }
    }

    pub fn item<T: Display>(mut self, text: T) -> Self {
        self.add_item(text);
        self
    }

    pub fn selected(&self) -> usize { self.selected_idx }

    pub fn add_item<T: Display>(&mut self, text: T) {
        self.elements.push(text.to_string());
    }

    fn line_size(&self, font: &mut Glyphs) -> [f64; 2] {
        let indent_width = font.width(self.font_size, "  ").unwrap();
        let max_width = self.elements.iter()
            .map(|elem| font.width(self.font_size, elem).unwrap())
            .reduce(f64::max)
            .unwrap_or(0.)
        + indent_width;

        let max_height = self.elements.iter()
            .map(|elem| {
                elem.chars()
                    .map(|c| {
                        let rc = font.character(self.font_size, c).unwrap();
                        rc.top()
                    })
                    .reduce(f64::max)
                    .unwrap_or(0.)
            })
            .reduce(f64::max)
            .unwrap_or(0.);

            [max_width, max_height]
    }

    fn rect_size(&self, font: &mut Glyphs) -> [f64; 3] {
        let [max_width, max_height] = self.line_size(font);
        let num_lines = self.elements.len() as f64;
        let height = max_height * num_lines;
        let rect_width = max_width + (max_height / 4.);
        let rect_height = height + (max_height / 2.);
        let spacing = (rect_height - height) / 2.;
        [rect_width, rect_height + (spacing * (num_lines - 1.)), spacing]
    }
}

impl Widget for SelectWidget {
    fn draw(&self, x: f64, y: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        let indent_width = font.width(self.font_size, "  ").unwrap();
        let [max_width, max_height] = self.line_size(font);
        let [rect_width, rect_height, spacing] = self.rect_size(font);
        
        Rectangle::new(self.box_color)
            .draw([x, y, rect_width, rect_height], &DrawState::default(), ctx.transform, gfx);

        let pos = [x + ((rect_width - max_width) / 2.), y + spacing];
        let text = Text::new_color(self.text_color, self.font_size);

        for (i, line) in self.elements.iter().enumerate() {
            let txt_pos = [
                pos[0] + indent_width,
                pos[1] + (i as f64 * (max_height + (spacing * (i > 0) as usize as f64))) + max_height
            ];

            text.draw_pos(line, txt_pos, font, &DrawState::default(), ctx.transform, gfx).unwrap();
        }

        font.factory.encoder.flush(dev);
        let yofs = self.selected_idx as f64 * (max_height + (spacing * (self.selected_idx > 0) as usize as f64));

        polygon(
            self.arrow_color,
            &[
                [x + spacing, y + spacing + yofs],
                [x + spacing, y + spacing + yofs + max_height],
                [
                    x + spacing + (max_height / 2.),
                    y + spacing + yofs + (max_height / 2.)
                ]
            ],
            ctx.transform, gfx
        );
    }

    fn on_event(&mut self, event: &Event, _tc: &mut G2dTextureContext, _font: &mut Glyphs) {
        if let Some(Button::Keyboard(key)) = event.press_args() {
            match key {
                Key::Down => {
                    self.selected_idx += 1;
                    if self.selected_idx >= self.elements.len() {
                        self.selected_idx = 0;
                    }
                }

                Key::Up => {
                    if self.selected_idx == 0 {
                        self.selected_idx = self.elements.len() - 1;
                    }
                    else {
                        self.selected_idx -= 1;
                    }
                }

                _ => { }
            }
        }
    }

    fn size(&self, _max: [f64; 2], font: &mut Glyphs) -> [f64; 2] {
        let [rect_width, rect_height, _] = self.rect_size(font);
        [rect_width, rect_height]
    }
}