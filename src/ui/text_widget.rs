use std::fmt::Display;
use gfx_device_gl::Device;
use piston_window::{
    types::Color,
    CharacterCache,
    Context,
    DrawState,
    G2d,
    Glyphs,
    Rectangle,
    Text
};
use super::Widget;

pub struct TextWidget {
    text: String,
    text_color: Color,
    font_size: u32,
    box_color: Option<Color>
}

impl TextWidget {
    pub fn new<T: Display>(text: T, text_color: Color, font_size: u32, box_color: Option<Color>) -> Self {
        TextWidget {
            text: text.to_string(),
            text_color,
            font_size,
            box_color
        }
    }

    fn line_size(&self, font: &mut Glyphs) -> [f64; 2] {
        let max_width = self.text.lines()
            .map(|line| font.width(self.font_size, line).unwrap())
            .reduce(f64::max)
            .unwrap_or(0.);

        let max_height = self.text.lines()
            .map(|line| {
                line.chars()
                    .map(|c| {
                        let rc = font.character(self.font_size, c).unwrap();
                        rc.top()
                    })
                    .reduce(f64::max)
                    .unwrap_or(0.)
            })
            .reduce(f64::max)
            .unwrap_or(0.);

        [max_width, max_height]
    }

    fn rect_size(&self, font: &mut Glyphs) -> [f64; 3] {
        let [max_width, max_height] = self.line_size(font);
        let num_lines = self.text.lines().count() as f64;
        let total_txt_height = max_height * num_lines;
        let height = max_height * num_lines;
        let rect_width = max_width + (max_height / 4.);
        let rect_height = height + (max_height / 2.);
        let spacing = (rect_height - total_txt_height) / 2.;
        [rect_width, rect_height + (spacing * (num_lines - 1.)), spacing]
    }
}

impl Widget for TextWidget {
    fn draw(&self, x: f64, y: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        let [max_width, max_height] = self.line_size(font);
        let [rect_width, rect_height, spacing] = self.rect_size(font);
        
        if let Some(color) = self.box_color {
            Rectangle::new(color)
                .draw([x, y, rect_width, rect_height], &DrawState::default(), ctx.transform, gfx);
        }

        let pos = [x + ((rect_width - max_width) / 2.), y + spacing + max_height];
        let text = Text::new_color(self.text_color, self.font_size);

        for (i, line) in self.text.lines().enumerate() {
            let txt_pos = [
                pos[0],
                pos[1] + (i as f64 * (max_height + (spacing * (i > 0) as usize as f64)))
            ];

            text.draw_pos(line, txt_pos, font, &DrawState::default(), ctx.transform, gfx).unwrap();
        }

        font.factory.encoder.flush(dev);
    }

    fn size(&self, _max: [f64; 2], font: &mut Glyphs) -> [f64; 2] {
        let [max_width, max_height] = self.line_size(font);
        let num_lines = self.text.lines().count() as f64;
        let height = max_height * num_lines;
        if self.box_color.is_some() {
            let [rect_width, rect_height, _] = self.rect_size(font);
            [rect_width, rect_height]
        }
        else { return [max_width, height]}
    }
}