use std::{collections::HashMap, ops::{AddAssign, Deref}};
use image::imageops::FilterType;
use opengl_graphics::TextureSettings;
use piston_window::{
    color, image as draw_image, rectangle, types::Color, CharacterCache, Context, G2d, G2dTexture, G2dTextureContext, Glyphs, Texture, Transformed
};
use gfx_device_gl::Device;
use serde::{Deserialize, Serialize};
use crate::{
    game,
    options::ColorBlindMode,
};

static BIG_TILES: [&str; 8] = [
    "1024.png",
    "2048.png",
    "4096.png",
    "8192.png",
    "16384.png",
    "32768.png",
    "65536.png",
    "131072.png"
];

pub type BigTileCache = Vec<G2dTexture>;

#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Tile(usize);

impl Tile {
    pub fn new(val: usize) -> Self {
        Tile(val)
    }

    pub fn is_empty(&self) -> bool { self.0 == 0 }
    pub fn is_2048(&self) -> bool { self.0 == 2048 }

    pub fn draw(&self,
        tile_x: usize,
        tile_y: usize,
        board_x: f64,
        board_y: f64,
        tile_size: f64,
        font: &mut Glyphs,
        ctx: Context,
        gfx: &mut G2d,
        dev: &mut Device,
        tile_cache: &BigTileCache,
        font_sizes: &HashMap<usize, u32>
    ) {
        let font_size = font_sizes.get(&self.0).copied().unwrap_or(1);
        // the the value of the tile is 0, do not render it
        if self.is_empty() {
            return;
        }

        let x = board_x + (tile_x as f64 * (tile_size + 5.)) + 2.5;
        let y = board_y + (tile_y as f64 * (tile_size + 5.)) + 2.5;

        let num_str = self.0.to_string();
        let height = font.character(font_size, num_str.chars().next().unwrap()).unwrap().top();

        if self.0 > 512 {
            let mut big_tile_idx = (self.0 as f64).log2() as usize - 10;
            if big_tile_idx > 7 { big_tile_idx = 0; }
            draw_image(&tile_cache[big_tile_idx], ctx.transform.trans(x,y), gfx);
        }
        else {
            rectangle(get_color(self.0, ColorBlindMode::Off), [x as f64, y as f64, tile_size, tile_size], ctx.transform, gfx);
            let width = font.width(font_size, &num_str).unwrap();
            let pos = [x + ((tile_size - width) / 2.), y + height as f64 + ((tile_size - height as f64) / 2.)];
            game::show_text(pos[0], pos[1], num_str, color::hex("51757A"), font_size, font, ctx, gfx, dev);
        }
    }
}

impl PartialEq for Tile {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for Tile { }

impl AddAssign<Tile> for Tile {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
    }
}

impl Deref for Tile {
    type Target = usize;
    fn deref(&self) -> &Self::Target { &self.0 }
}

// gets the color for a specific tile number
fn get_color(tile: usize, mode: ColorBlindMode) -> Color {
    let colors = match mode {
        ColorBlindMode::Off =>
            [color::hex("BFC3C7"), color::hex("79A7A8"), color::hex("56A7C7"), color::hex("1278A1"), color::hex("0098DE"), color::hex("715BDE"), color::hex("7B2FDE"), color::hex("A300dE"), color::hex("7E00DE"), color::hex("5900DE"), [0., 0., 0., 1.]],

        ColorBlindMode::RedGreen =>
            [color::hex("ADADAD"), color::hex("00A7A8"), color::hex("14A7C7"), color::hex("1278A1"), color::hex("0078A1"), color::hex("4225FF"), color::hex("6966FF"), color::hex("7F6DFF"), color::hex("000ADE"), color::hex("1400DE"), [0., 0., 0., 1.]],

        ColorBlindMode::BlueYellow =>
            [color::hex("BFC3C7"), color::hex("79A7A8"), color::hex("BD56C7"), color::hex("BD129F"), color::hex("BD98DE"), color::hex("BD5BDE"), color::hex("BD2FDE"), color::hex("A300DE"), color::hex("7E00DE"),  color::hex("BD00DE"), [0., 0., 0., 1.]]
    };

    let mut tile_idx = (tile as f64).log2() as usize;
    if tile_idx > 10 { tile_idx = 10; }
    colors[tile_idx]
}

// load all of the big number (1024+) tile images into memory all at once
pub fn gen_tile_cache(tc: &mut G2dTextureContext, board_size: usize, real_size: u32) -> Vec<G2dTexture> {
    let mut tiles = Vec::new();
    let tiles_dir = game::get_game_dir().join("assets/tiles/");

    for big_tile in BIG_TILES {
        let size = (real_size / board_size as u32) - 5;
        let img = image::open(tiles_dir.join(big_tile)).unwrap().resize(size, size, FilterType::Gaussian);
        let tile = img.to_rgba8();
        tiles.push(Texture::from_image(tc, &tile, &TextureSettings::new()).unwrap());
    }

    tiles
}