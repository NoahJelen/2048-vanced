use std::{env, fs, iter::Enumerate, ops::Range, path::PathBuf, slice::Iter};
use piston_window::Key;
use rand::{rngs::ThreadRng, thread_rng, Rng};
use serde::{Deserialize, Serialize};

use crate::tiles::Tile;

type Field = Vec<Vec<Tile>>;

#[derive(Clone, Serialize, Deserialize)]
pub struct GameData {
    score: usize,
    best: usize,
    board: Board,
    board_size: usize,
    saw_won_scr: bool
}

impl GameData {
    fn new(board_size: usize) -> Self {
        GameData {
            score: 0,
            best: 0,
            board: Board::new(board_size),
            saw_won_scr: false,
            board_size,
        }
    }

    pub fn iter_rows(&self) -> Enumerate<Iter<Vec<Tile>>> {
        self.board.field.iter().enumerate()
    }

    pub fn undo(&mut self) { self.board.undo(); }

    pub fn get_formatted_best(&self) -> String {
        format!("Best Score: {}", self.best)
    }

    pub fn score(&self) -> usize { self.score }

    pub fn best(&self) -> usize { self.best }

    pub fn reset(&mut self) {
        self.board = Board::new(self.board_size);
        self.score = 0;
        self.save();
    }

    // let the user make a move and add points to score
    pub fn do_move(&mut self, key: Key) {
        let umove = Usermove::from(key);
        self.score += self.board.do_move(umove);

        // is the score greater than the best score?
        // if so, update the best score
        self.update_best();

        // generate new tiles if the move was valid
        if self.board.has_moved() {
            self.board.new_tile();
        }

        // did the user win the game and dismiss the game won message?
        if !self.saw_won_scr {
            self.saw_won_scr = self.won() && key == Key::Escape;
        }

        // save the game to a file
        self.save();
    }

    // can the use make anymore moves?
    pub fn is_over(&self) -> bool {
        let mut test_board = self.board.clone();
        let moves = [Usermove::Up, Usermove::Down, Usermove::Left, Usermove::Right];
        for umove in moves {
            test_board.do_move(umove);
            if test_board.field != self.board.field {
                return false;
            }
            match umove {
                Usermove::Right => return true,
                _ => { }
            }
        }

        true
    }

    pub fn won(&self) -> bool {
        for row in self.board.field.iter() {
            if row.iter().any(Tile::is_2048) {
                return true;
            }
        }

        false
    }

    pub fn save(&self) {
        let data_dir = PathBuf::from(
            env::var("XDG_DATA_HOME")
                .unwrap_or(format!("{}/.local/share/2048-vanced", env::var("HOME").unwrap()))
        );

        let save_file = data_dir.join(format!("save.{}", self.board_size));
        fs::create_dir_all(&data_dir).unwrap_or(());
        let encoded = bincode::serialize(&self).unwrap();
        fs::write(save_file, encoded).unwrap();
    }

    pub fn load(board_size: usize) -> Self {
        let data_dir = PathBuf::from(
            env::var("XDG_DATA_HOME")
                .unwrap_or(format!("{}/.local/share/", env::var("HOME").unwrap()))
        )
            .join("2048-vanced");

        let load_file = data_dir.join(format!("save.{}", board_size));

        if let Ok(encoded) = fs::read(load_file) {
            bincode::deserialize(&encoded).unwrap_or(Self::new(board_size))
        }
        else {
            Self::new(board_size)
        }
    }

    pub fn update_best(&mut self) {
        if self.score > self.best {
            self.best = self.score;
        }
    }

    pub fn size(&self) -> usize { self.board_size }

    pub fn saw_won_scr(&self) -> bool { self.saw_won_scr }
}

//the board of numbers with some helper methods attached
#[derive(Clone, Serialize, Deserialize)]
struct Board {
    // the field of numbers, as a square matrix vector
    field: Field,

    // the field of numbers from the previous move
    prev: Field,

    #[serde(skip)]
    rand: ThreadRng
}

impl Board {
    fn new(size: usize) -> Board {
        let field = vec![vec![Tile::new(0); size]; size];
        let prev = field.clone();
        let mut board = Board {
            field,
            prev,
            rand: thread_rng()
        };

        // add 2 new tiles to the new board
        board.new_tile();
        board.new_tile();
        board
    }

    fn new_tile(&mut self) {
        let size = self.field.len();
        for _ in 0..size ^ 2 {
            let x = self.rand.gen::<usize>();
            if self.field[x % size][(x / size) % size].is_empty() {
                if x % 5 == 0 {
                    self.field[x % size][(x / size) % size] = Tile::new(4);
                }
                else if x % 10 == 0 {
                    self.field[x % size][(x / size) % size] = Tile::new(2);
                }
                else {
                    self.field[x % size][(x / size) % size] = Tile::new(1);
                }

                break;
            }
        }
    }

    fn do_move(&mut self, usermove: Usermove) -> usize {
        self.prev = self.field.clone();
        usermove.process(&mut self.field)
    }

    fn undo(&mut self) { self.field = self.prev.clone(); }

    fn has_moved(&self) -> bool { self.prev != self.field }
}

// possible moves for the player
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Usermove {
    Up,
    Down,
    Left,
    Right,
    None
}

impl Usermove {
    // move the field based on the move
    fn process(self, field: &mut Field) -> usize {
        if self == Self::None { return 0; }
        let reverse = matches!(self, Self::Right | Self::Down);
        let size = field.len();
        let mut score = 0;

        // is this a vertical or horizontal move?
        let is_vertical = matches!(self, Self::Up | Self::Down);

        if is_vertical {
            for col in 0..size {
                for row in rev_range(0..size, reverse) {
                    for testrow in rev_range(
                        if reverse { 0..row }
                        else { (row + 1)..size },
                        reverse
                    ) {
                        let test_space = field[testrow][col];
                        let space = field[row][col];
                        if !test_space.is_empty() {
                            if space.is_empty() {
                                field[row][col] += test_space;
                                field[testrow][col] = Tile::new(0);
                            }
                            else if space == test_space {
                                field[row][col] += test_space;
                                field[testrow][col] = Tile::new(0);
                                score += *test_space;
                                break;
                            }
                            else { break; }
                        }
                    }
                }
            }

            score * 2
        }
        else {
            for row in field {
                for col in rev_range(0..size, reverse) {
                    for testcol in rev_range(
                        if !reverse { (col + 1)..size }
                        else { 0..col },
                        reverse
                    ) {
                        let test_space = row[testcol];
                        let space = row[col];
                        if !test_space.is_empty() {
                            if space.is_empty() {
                                row[col] += test_space;
                                row[testcol] = Tile::new(0);
                            }
                            else if space == test_space {
                                row[col] += test_space;
                                row[testcol] = Tile::new(0);
                                score += *test_space;
                                break;
                            }
                            else { break }
                        }
                    }
                }
            }

            score * 2
        }
    }
}

impl From<Key> for Usermove {
    fn from(key: Key) -> Self {
        match key {
            Key::Left | Key::A => Usermove::Left,
            Key::Up | Key::W => Usermove::Up,
            Key::Down | Key::S => Usermove::Down,
            Key::Right | Key::D => Usermove::Right,
            _ => Usermove::None
        }
    }
}

// create an iterator that is optionally reversible
fn rev_range(range: Range<usize>, reverse: bool) -> Box<dyn Iterator<Item = usize>> {
    if reverse {
        Box::new(range.rev())
    }
    else { Box::new(range) }
}