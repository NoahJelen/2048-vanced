use std::{
    env, fmt::Display, fs, path::PathBuf
};
use gfx_device_gl::Device;
use piston_window::{
    types::Color,
    Context, G2d,
    Glyphs,
    Transformed
};

mod selector;
mod data;
mod board;

pub use selector::BoardSelector;
pub use board::BoardWidget;

pub fn show_text<T: Display>(x: f64, y: f64, text: T, color: Color, size: u32, font: &mut Glyphs, c: Context, g: &mut G2d, d: &mut Device) {
    let text = text.to_string();
    piston_window::text(color, size, &text, font, c.transform.trans_pos([x, y]), g).expect("Unable to render text!");
    font.factory.encoder.flush(d);
}

pub fn get_game_dir() -> PathBuf {
    let mut game_dir = env::current_exe().unwrap();
    game_dir.pop();
    if fs::read_dir(game_dir.join("assets")).is_err() {
        game_dir = env::current_dir().unwrap();
    }

    game_dir
}