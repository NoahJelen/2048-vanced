use gfx_device_gl::Device;
use opengl_graphics::TextureSettings;
use piston_window::{
    image as draw_image,
    polygon, rectangle,
    types::Color,
    Button, Context,
    Event, Flip,
    G2d, G2dTexture,
    G2dTextureContext,
    Glyphs, Key, PressEvent, Texture, Transformed
};
use crate::{game::data::GameData, ui::{
    TextWidget,
    Widget
}};

pub struct BoardSelector {
    size: usize,
    text_color: Color,
    box_color: Color,
    arrow_color: Color,
    preview_image: Option<G2dTexture>
}

impl BoardSelector {
    pub fn new(text_color: Color, box_color: Color, arrow_color: Color) -> Self {
        BoardSelector {
            size: 4,
            preview_image : None,
            text_color,
            box_color,
            arrow_color
        }
    }

    pub fn cycle_boards(&mut self, left: bool) {
        if left {
            self.size -= 1;
            if self.size < 4 {
                self.size = 7;
            }
        }
        else {
            self.size += 1;
            if self.size > 7 {
                self.size = 4;
            }
        }

        self.preview_image = None;
    }

    pub fn refresh_image(&mut self, tc: &mut G2dTextureContext) -> bool {
        let image_file = super::get_game_dir().join("assets/board_sizes/").join(format!("{0}x{0}.png", self.size));
        if self.preview_image.is_none() {
            self.preview_image = Texture::from_path(
                tc,
                &image_file,
                Flip::None,
                &TextureSettings::new()
            )
                .ok();

            return true;
        }

        false
    }

    pub fn size(&self) -> usize { self.size }

    pub fn get_best_score(&self) -> String {
        let data = GameData::load(self.size);
        data.get_formatted_best()
    }
}

impl Widget for BoardSelector {
    fn draw(&self, x: f64, y: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        // central box
        rectangle(self.box_color, [x + 89., y, 380., 375.], ctx.transform, gfx);

        // board preview
        if let Some(ref preview_image) = self.preview_image {
            draw_image(preview_image, ctx.transform.trans(x + 156., y + 20.), gfx);
        }

        let text = TextWidget::new(format!("{0}x{0} Square", self.size), self.text_color, 50, None);
        text.draw(x + 94., y + 305., font, ctx, gfx, dev);

        // left arrow
        polygon(
            self.arrow_color,
            &[
                [x + 64., y + 105.],
                [x + 64., y + 233.],
                [x, y + 169.]
            ],
            ctx.transform, gfx
        );

        // right arrow
        polygon(
            self.arrow_color,
            &[
                [x + 494., y + 105.],
                [x + 494., y + 233.],
                [x + 558., y + 169.]
            ],
            ctx.transform, gfx
        )
    }

    fn on_event(&mut self, event: &Event, _tc: &mut G2dTextureContext, _font: &mut Glyphs) {
        if let Some(Button::Keyboard(key)) = event.press_args() {
            match key {
                Key::Left => self.cycle_boards(true),
                Key::Right => self.cycle_boards(false),
                _ => { }
            }
        }
    }

    fn size(&self, _max: [f64; 2], _font: &mut Glyphs) -> [f64; 2] { [558., 375.] }
}