use std::{cell::Cell, collections::HashMap};
use gfx_device_gl::Device;
use piston_window::{
    rectangle, Button, CharacterCache, Context, Event, G2d, G2dTextureContext, Glyphs, Key, PressEvent
};
use crate::{
    tiles::{self, BigTileCache}, ui::Widget, SILVER
};
use super::data::GameData;

pub struct BoardWidget {
    data: GameData,
    size: Cell<f64>,
    tile_size: f64,
    font_sizes: HashMap<usize, u32>,
    tile_cache: BigTileCache
}

impl BoardWidget {
    pub fn new(board_size: usize) -> Self {
        let data = GameData::load(board_size);
        BoardWidget {
            data,
            size: Cell::new(0.),
            tile_size: 0.,
            font_sizes: HashMap::new(),
            tile_cache: vec![]
        }
    }

    pub fn data(&self) -> &GameData { &self.data }
    pub fn data_mut(&mut self) -> &mut GameData { &mut self.data }

    pub fn reload(&mut self, board_size: usize) {
        self.data = GameData::load(board_size);
        self.size.set(0.);
        self.tile_cache.clear();
    }

    fn refresh_tiles(&mut self, font: &mut Glyphs) {
        self.tile_size = (self.size.get() / self.data.size() as f64) - 5.0;
        for i in 0..18 {
            let num = 2usize.pow(i);
            self.font_sizes.insert(num, get_font_size(num, self.tile_size, font));
        }
    }
}

impl Widget for BoardWidget {
    fn draw(&self, x: f64, y: f64, font: &mut Glyphs, ctx: Context, gfx: &mut G2d, dev: &mut Device) {
        let size = self.size.get();

        // generate the board background
        rectangle(SILVER, [x, y, size, size], ctx.transform, gfx);

        // when the big tile cache has been generated, render the tiles
        if !self.tile_cache.is_empty() {
            for (tile_y, row) in self.data.iter_rows() {
                for (tile_x, tile) in row.iter().enumerate() {
                    tile.draw(
                        tile_x,
                        tile_y,
                        x,
                        y,
                        self.tile_size,
                        font,
                        ctx,
                        gfx,
                        dev,
                        &self.tile_cache,
                        &self.font_sizes
                    );
                }
            }
        }
    }

    fn on_event(&mut self, event: &Event, _tc: &mut G2dTextureContext, _font: &mut Glyphs) {
        if let Some(Button::Keyboard(key)) = event.press_args() {
            match key {
                Key::R => self.data.reset(),
                Key::U => self.data.undo(),
                key => self.data.do_move(key)
            }
        }
    }

    fn resize(&mut self, new_max: [f64; 2], tc: &mut G2dTextureContext, font: &mut Glyphs) {
        let size = get_shortest_side(new_max);
        self.size.set(size);
        self.tile_size = (size / self.data.size() as f64) - 5.0;
        self.tile_cache = tiles::gen_tile_cache(tc, self.data.size(), size as u32);
        self.refresh_tiles(font);
    }

    fn size(&self, max: [f64; 2], _font: &mut Glyphs) -> [f64; 2] {
        let size = get_shortest_side(max);
        self.size.set(size);
        [size; 2]
    }
}

fn get_shortest_side(size: [f64; 2]) -> f64 {
    let [width, height] = size;
    let side = if height > width {
        width
    }
    else {
        height
    };
    side - 10.
}

fn get_font_size(num: usize, tile_size: f64, font: &mut Glyphs) -> u32 {
    let num_str = num.to_string();
    let mut size = tile_size as u32 / 2;
    let width = font.width(size, &num_str).unwrap();
    if width > tile_size {
        while size > 1 {
            let width = font.width(size, &num_str).unwrap();
            if width > tile_size {
                size -= 1;
            }
            else {
                break;
            }
        }
    }

    size
}