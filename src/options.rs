use rust_utils::config::{Config, ConfigType};
use serde::{Serialize, Deserialize};
use std::env;

#[derive(Clone, Serialize, Deserialize)]
pub struct Options {
    // boards are always square, so only one value is needed
    pub board_size: usize,
    pub color_blind_mode: ColorBlindMode
}

impl Default for Options {
    fn default() -> Self {
        Options {
            board_size: 4,
            color_blind_mode: ColorBlindMode::Off
        }
    }
}

impl Config for Options {
    const FILE_NAME: &'static str = "options.ron";
    const TYPE: ConfigType = ConfigType::Ron;

    fn get_save_dir() -> String {
        let data_dir = env::var("XDG_CONFIG_HOME")
            .unwrap_or(format!("{}/.config/", env::var("HOME").unwrap()));

        format!("{data_dir}/2048-vanced")
    }
}

#[derive(Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum ColorBlindMode {
    Off,
    RedGreen,
    BlueYellow
}

impl ColorBlindMode {
    pub fn next(&self) -> ColorBlindMode {
        match *self {
            ColorBlindMode::Off => ColorBlindMode::RedGreen,
            ColorBlindMode::RedGreen => ColorBlindMode::BlueYellow,
            ColorBlindMode::BlueYellow => ColorBlindMode::Off
        }
    }

    pub fn prev(&self) -> ColorBlindMode {
        match *self {
            ColorBlindMode::Off => ColorBlindMode::BlueYellow,
            ColorBlindMode::RedGreen => ColorBlindMode::Off,
            ColorBlindMode::BlueYellow => ColorBlindMode::RedGreen
        }
    }

    pub fn desc(&self) -> String {
        match *self {
            ColorBlindMode::Off => "Off".to_string(),
            ColorBlindMode::RedGreen => "Red-Green".to_string(),
            ColorBlindMode::BlueYellow => "Blue-Yellow".to_string()
        }
    }
}